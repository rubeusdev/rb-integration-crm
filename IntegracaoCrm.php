<?php
namespace Rubeus\IntegracaoCrm;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Servicos\Entrada\I;

class IntegracaoCrm
{
    
    private $token;
    private $canal;
    private $url;
    
    public function __construct()
    {
        $this->token = CRM_TOKEN;
        $this->canal = CRM_CANAL;
        $this->url = CRM_URL;
    }
    
    public function enviar($dados, $metodo, $naoPassarTracking = false)
    {
        $dados['token'] = $this->token;
        $dados['origem'] = $this->canal;
        if (is_null($dados['idPessoaCrm']) || $dados['idPessoaCrm'] == 0) {
            unset($dados['idPessoaCrm']);
        }
        if (is_null($dados['id']) || $dados['id'] == 0) {
            unset($dados['id']);
        }

        if ($metodo == 'Pessoa/cadastro' && I::get('hashRBTrancking', I::post('hashRBTrancking')) && !$naoPassarTracking) {
            if (defined('UTILIZAR_API_ANTIGA_TRACKING') && UTILIZAR_API_ANTIGA_TRACKING == 1) {
                if (defined('LINK_API_TRACKING')) {
                    $ch = curl_init(LINK_API_TRACKING.'/identify');
                } else {
                    $ch = curl_init('https://tracking.apprubeus.com.br/identify');
                }
                $metodo = 'tracking/identify';
            } else {
                if (defined('LINK_API_TRACKING')) {
                    $ch = curl_init(LINK_API_TRACKING.'/api/v2/sendData');
                } else {
                    $ch = curl_init('https://tracking.apprubeus.com.br/api/v2/sendData');
                }
                $metodo = 'tracking/v2/sendData';
            }
            unset($dados['codigo']);
            $dados['session_id'] = I::get('hashRBTrancking', I::post('hashRBTrancking'));
            $dados['substitute_id'] = I::get('subHashRBTrancking', I::post('subHashRBTrancking'));
            $dados['clientRubeus'] = I::get('clientHashRBTrancking', I::post('clientHashRBTrancking'));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-HTTP-Method-Override: PATCH', 'Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dados));
        } elseif ($metodo == 'Evento/cadastro' && I::get('hashRBTrancking', I::post('hashRBTrancking')) && !$naoPassarTracking) {
            if (defined('UTILIZAR_API_ANTIGA_TRACKING') && UTILIZAR_API_ANTIGA_TRACKING == 1) {
                if (defined('LINK_API_TRACKING')) {
                    $ch = curl_init(LINK_API_TRACKING.'/sendEvent');
                } else {
                    $ch = curl_init('https://tracking.apprubeus.com.br/sendEvent');
                }
                $metodo = 'tracking/sendEvent';
            } else {
                if (defined('LINK_API_TRACKING')) {
                    $ch = curl_init(LINK_API_TRACKING.'/api/v2/sendEvent');
                } else {
                    $ch = curl_init('https://tracking.apprubeus.com.br/api/v2/sendEvent');
                }
                $metodo = 'tracking/v2/sendEvent';
            }
            unset($dados['pessoa']);
            $dados = [
                'event' => [[
                    'eventData' => $dados,
                    'eventType' => $dados['tipo']
                    ]
                ],
                'clientRubeus' => I::get('clientHashRBTrancking', I::post('clientHashRBTrancking')),
                'session_id' => I::get('hashRBTrancking', I::post('hashRBTrancking'))
            ];
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-HTTP-Method-Override: PATCH', 'Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dados));
        } else {
            $ch = curl_init($this->url.'/api/'.$metodo);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($dados));
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        if (!$response) {
            $erro = curl_error($ch);
        }
        curl_close($ch);
        $this->registrarChamada($dados, $metodo, $response, $erro);
        return $response;
    }
    
    private function registrarChamada($dados, $metodo, $response, $erro){
        Conteiner::get('RegistrarChamadaCrm')->registrar(json_encode($dados), $metodo, $response, $erro);
    }
    
    public function enviarPessoa($dados, $naoPassarTracking = false){
        return $this->enviar($dados, 'Pessoa/cadastro', $naoPassarTracking);
    }
    
    public function enviarEvento($dados){
        return $this->enviar($dados, 'Evento/cadastro');
    }
    
    public function enviarTipoEvento($dados){
        return $this->enviar($dados, 'Evento/cadastroTipoEvento');
    }
    
    public function enviarCurso($dados){
        return $this->enviar($dados, 'Curso/cadastroCurso');
    }
    
    public function enviarOfertaCurso($dados){
        return $this->enviar($dados, 'Curso/cadastroOferta');
    }
    
    public function enviarProcessoSeletivo($dados){
        return $this->enviar($dados, 'ProcessoSeletivo/cadastro');
    }
    
    public function enviarLocais($dados){
        return $this->enviar($dados, 'LocalOferta/cadastro');
    }
    
    public function enviarCampoPersonalizado($dados){
        return $this->enviar($dados, 'Instituicao/adicionarCampo');
    }
    
    public function consultarCampoPersonalizado(){
        return $this->enviar($dados, 'Instituicao/campoPersonalizado');
    }

    public function consultarCampoPessoa($dados){
        return $this->enviar($dados, 'Contato/dadosPessoa');
    }
    
    public function consultarDadosPessoa($dados){
        return $this->enviar($dados, 'Pessoa/dadosPessoa');
    }
    
    public function alterarCodigoPessoa($dados){
        return $this->enviar($dados, 'Pessoa/alterarCodigo');
    }
    
    public function alterarStatus($dados){
        return $this->enviar($dados, 'Oportunidade/alterarStatus');
    }
    
    public function alterarPessoas($dados){
       return $this->enviar($dados, 'Oportunidade/alterarPessoasOportunidade');
   }
   
    public function excluirRegistrosEvento($dados){
       return $this->enviar($dados, 'Evento/excluirRegistros');
   }
   public function verificarEvento($dados){
       return $this->enviar($dados, 'Evento/verificarCadastro');
   }
}